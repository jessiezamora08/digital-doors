# Time tracking dashboard solution


### The challenge

Users should be able to:

- View the optimal layout for the site depending on their device's screen size
- See hover states for all interactive elements on the page
- Switch between viewing Daily, Weekly, and Monthly stats





Requirements

- for the data use the JSON file on this folder
- for the images use the images on the image folder( you can also add your own image)
-for the sample design see the design folder

-use HTML5
-CSS
-Must be responsive website
-use Javascript







